# JS-postForm1
 Just the post form with the HTML5 responsible design. 
Dynamically show what user wrote and send that to mail.
I use JS only. No frameworks.
## How to set the receiving mail?


```
    function sendMail()
        {
            if(checkTheForm())
                emailjs.send("default_service",
                    "templ_1",
                    {
                      "to_mail": "Yuriy@Lesyo.eu", // HERE WRITE YOUR MAIL AND YOU WILL GET ALL MAIL'S TO YOUR MAIL
                      "url": document.URL,
                      "message_html": document.getElementById('mail').innerText
                    }
                );
        }
```

## How to add rules for checking?
How to add rules for checking?
Now I only check phone number and age.
If you would like to check mails if it correct by more parameters, you should just add next lines:
```
if( CONDITION ) // For example, a message is empty. (message.value === EMPTY_STRING)
        messageText += "Here should be a error text\n"; // "\n" adds newline
```
Add you code to this function
```
    function checkTheForm()
    {
        let messageText = EMPTY_STRING;
        // errors
        if(Number(age.value) <= 0)
            messageText += "Please, write this form when will you born :D\n";
    
        if(phone.value.length !== 9 && phone.value.length !== 12)
            messageText += "Your phone number should has 9 numbers or 12.\n";
    
         // HERE SHOULD BE YOUR CONDITION
    
        if (messageText === EMPTY_STRING)
        {
            messageText = SUCCESSFULY_MESSAGE;
            document.getElementById('result').classList.remove("error");
            document.getElementById('result').classList.add("success");
        } else {
            document.getElementById('result').classList.remove("success");
            document.getElementById('result').classList.add("error");
        }
    
        document.getElementById('result').innerText = messageText;
        return messageText === SUCCESSFULY_MESSAGE;
    }
```

## DEMO
If you would like to see, how it works and looks, just go [there](http://lesyo.eu/gitLab/postForm1/).